import express from 'express'
import * as jwtGenerator from 'jsonwebtoken'
import entryRoutes from './src/entries'
import profile from './src/profile'
import careers from './src/careers'
import projects from './src/projects'
import cors from 'cors'

const mysql = require('mysql');
const app = express()

app.use(express.json())
app.use(cors())

const configuration = {
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
}
if (process.env.DATABASE_SOCKET) {
    configuration.socketPath = process.env.DATABASE_SOCKET
} else {
    configuration.host = process.env.DATABASE_HOST
}

app.use(function(req, res, next){
    global.connection = mysql.createConnection(configuration);
    connection.connect(function (err) {
        if (err) {
          console.error("error connecting: " + err.stack);
          return;
        }
      
        console.log("Database connected");
      });
    next();
});

app.post('/auth', (req, res) => {
    const username = req.body.username
    const password = req.body.password

    if (username == "test" && password == "password") {
        const token = jwtGenerator.sign({username}, process.env.JWT_SECRET, {expiresIn: '1h'})
        return res.send({token})
    }
    return res.status(401).send({error: "incorrect username\password"})
})

app.use('/contact_form/entries', entryRoutes)
app.use('/profile', profile)
app.use('/careers', careers)
app.use('/projects', projects)

app.listen(process.env.ENV_PORT || 3000, () => {
    console.log(`app is running on port ${process.env.ENV_PORT || 3000}`)
})
