import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO Careers (company_name, period, job_title, company_location, person_id) VALUES  (?, ?, ?, ?, ?)",
    [
      req.body.companyName,
      req.body.period,
      req.body.jobTitle,
      req.body.companyLocation,
      1,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM Careers_Responsibility WHERE career_id=${req.params.id};`,
    function (error, result1, fields) {
      if (error) throw error;
      connection.query(
        `DELETE FROM Careers WHERE career_id=${req.params.id};`,
        function (error, result2, fields) {
          if (error) throw error;
          return res
            .status(200)
            .send({ Careers_Responsibility: result1, careers: result2 });
        }
      );
    }
  );
});

router.put("/:id", (req, res) => {
  const { companyName, periodTime, jobTitle, companyLocation } = req.body;
  connection.query(
    `UPDATE Careers SET company_name="${companyName}", period="${periodTime}", job_title="${jobTitle}", company_location="${companyLocation}"  WHERE career_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.get("/", (req, res) => {
  connection.query(
    "SELECT Careers.career_id, Careers.company_name, Careers.company_image, Careers.period, Careers.job_title, Careers.company_location, Careers_Responsibility.responsibility_desc From Careers LEFT JOIN Careers_Responsibility ON Careers.career_id = Careers_Responsibility.career_id",
    function (error, results, fields) {
      if (error) throw error;
      let newResults = [];
      let career_id;
      for (let i = 0; i < results.length; i++) {
        if (career_id != results[i].career_id) {
          results[i].responsibilities = [];
          for (let careerItem of results) {
            if (results[i].career_id === careerItem.career_id) {
              results[i].responsibilities.push(careerItem.responsibility_desc);
            }
          }
          delete results[i].responsibility_desc;
          newResults.push(results[i]);
          career_id = results[i].career_id;
        }
      }

      return res.status(200).send(newResults);
    }
  );
});

router.get("/:id", (req, res) => {
  connection.query(`SELECT * FROM Careers WHERE career_id=${req.params.id}`, function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});

export default router;
