import express from "express";

const router = express.Router();

router.get("/", (req, res) => {
  connection.query("SELECT * FROM Projects", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});

router.get("/:id", (req, res) => {
  connection.query(`SELECT * FROM Projects WHERE project_id=${req.params.id}`, function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO Projects (project_name, project_image, project_imageAlt, project_desc, project_tag, repo_url, person_id) VALUES  (?, ?, ?, ?, ?, ?, ?)",
    [
      req.body.projectname,
      req.body.projectImage,
      req.body.projectname,
      req.body.projectdesc,
      req.body.projectTag,
      req.body.repoUrl,
      1
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM Projects WHERE project_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.put("/:id", (req, res) => {
  const { projectname, projectImage, projectdesc, projectTag,repoUrl  } = req.body;
  connection.query(
    `UPDATE Projects SET project_name="${projectname}", project_desc="${projectdesc}", project_tag="${projectTag}", repo_url="${repoUrl}", project_image="${projectImage}", project_imageAlt="${projectname}"  WHERE project_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;
