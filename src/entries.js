import express from "express";
import jwt from "express-jwt";

const router = express.Router();

const validateEntries = (req, res, next) => {
  const body = req.body;
  const invalid = [];
  console.log(body);
  for (var property in body) {
    console.log(property);
    if (body[property] == null || body[property].length === 0) {
      invalid.push(property);
    }
  }
  const requiredProperties = ["firstName", "email", "subject", "comments"];
  requiredProperties
    .filter((prop) => !body.hasOwnProperty(prop))
    .forEach((key) => invalid.push(key));
  if (invalid.length > 0) {
    return res.status(400).send({ message: "validation error", invalid });
  }
  next();
};

router.post("/", validateEntries, (req, res) => {
  connection.query(
    "INSERT INTO Contacts (first_name, last_name, email, phone_number, subject, comments, person_id) VALUES (?, ?, ?, ?, ?, ?, ?)",
    [
      req.body.firstName,
      req.body.lastName,
      req.body.email,
      req.body.contatNo,
      req.body.subject,
      req.body.comments,
      1,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.use(jwt({ secret: process.env.JWT_SECRET }));

router.get("/", (req, res) => {
  connection.query("SELECT * FROM Contacts", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});

export default router;
