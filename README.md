1. To run on local, create .env file with below information, 
JWT_SECRET=test
JWT_SECRET=test
DATABASE_HOST=db
DATABASE_ROOT_PASSWORD=testroot
DATABASE_USER=testuser
DATABASE_PASSWORD=mysqltest
DATABASE_NAME=test
2. Database connection on local: your will need to create a new user "testuser" and password "mysqltest" and database "test"
3. You will need to run below sql commands on your local:
CREATE DATABASE `test`;

USE `test`;

CREATE TABLE IF NOT EXISTS Profiles (
    person_id int NOT NULL AUTO_INCREMENT,
    first_name varchar(255),
    last_name varchar(255),
    hobbies varchar(255),
    city varchar(255),
    province varchar(255),
    social_media_url varchar(255),
    self_desc varchar(255),
    about_me varchar(1000),
    PRIMARY KEY (person_id)
);

INSERT INTO Profiles (first_name, last_name, hobbies, city, province, social_media_url, self_desc, about_me )
VALUES 
("Serena", "Fan", "kayaking and paddle boarding, hiking and traveling, a foodie", "Markham", "Ontario", "https://www.linkedin.com/in/serena-fan-91b3511b2/", "I'm a creative software QA Analyst", "I'm a QA Engineer with a passion for software development and testing. I'm a Knowledgeable and efficiency-obsessed professional with 6+ years of progressive experience helping engineering teams design, develop, and maintain sustainable automated test suites and incorporate exploratory testing into delivery process. Communicative team player with analytical troubleshooting skills, collaborates with stakeholders and balances multiple projects/tasks.");

CREATE TABLE IF NOT EXISTS Projects (
    project_id int NOT NULL AUTO_INCREMENT,
    project_name varchar(255),
	project_image varchar(100),
	project_imageAlt varchar(100),
    project_desc varchar(255),
    project_tag varchar(255),
    repo_url varchar(255),
    person_id int NOT NULL,
    PRIMARY KEY (project_id),
    FOREIGN KEY (person_id) REFERENCES Profiles (person_id)
);


INSERT INTO Projects (project_name, project_image, project_imageAlt, project_desc, project_tag, repo_url, person_id)
VALUES 
("Mobile Automation Testing", "https://i.ibb.co/VS1pHbC/appium.png", "appium", "Build automation scripts for mobile hybrid iOS and Android apps", "Appium Java TestNG Maven TDD", "https://github.com/serenafan/demoAppiumAndroid", 1),
("Prosonal Profile Website", "https://i.ibb.co/zmY6t6J/personalwebsite.png", "personalwebsite", "A personal website to showcase projects and experiences", "HTML CSS Javascript", "https://github.com/serenafan/Fall2020FS1000/tree/master", 1),
("Web Automation Testing", "https://i.ibb.co/km0jskx/cypress.png", "cypress", "Build E2E test scripts for web automation on Chrome and Firefox", "Cypress Javascript Cucumber BDD", "https://github.com/serenafan/Cypress-project", 1);

CREATE TABLE IF NOT EXISTS Careers (
    career_id int NOT NULL AUTO_INCREMENT,
    company_name varchar(255),
    company_image varchar(100),
    period varchar(255),
    job_title varchar(255),
    company_location varchar(255),
    person_id int NOT NULL,
    PRIMARY KEY (career_id),
    FOREIGN KEY (person_id) REFERENCES Profiles (person_id)
);


INSERT INTO Careers (company_name, company_image, period, job_title, company_location, person_id)
VALUES 
("Freshii", "https://i.ibb.co/23Z2Kb1/freshii.png", "Jul 2019 to Present", "Automation Test Engineer", "Toronto • ON", 1),
("State Street", "https://i.ibb.co/Brb9jFF/statestreet.png", "Aug 2018 to Jun 2019", "Senior QA Analyst", "Hangzhou • China", 1),
("DealTap", "https://i.ibb.co/jLNh2Xw/dealtap.png", "Jan 2017 to Jun 2018", "Senior QA Analyst", "Toronto • ON", 1),
("Rogers", "https://i.ibb.co/wdZv5dQ/rogers.png", "Feb 2015 to Dec 2016", "QA Analyst", "Brampton • ON", 1),
("Capgemini", "https://i.ibb.co/tzXwq4L/capgemini.png", "Jun 2012 to Apr 2014", "QA Analyst/System Analyst", "Hangzhou • China", 1);

CREATE TABLE IF NOT EXISTS Careers_Responsibility (
    responsibility_id int NOT NULL AUTO_INCREMENT,
    responsibility_desc varchar(1000),
    career_id int NOT NULL,
    PRIMARY KEY (responsibility_id),
    FOREIGN KEY (career_id) REFERENCES Careers (career_id)
);

INSERT INTO Careers_Responsibility (responsibility_desc, career_id)
VALUES 
("Work in a highly dynamic Agile environment, participate in project user stories backlog grooming, business requirement intakes and sign-off, attend sprint tasks rating, stand-up meetings, business acceptance demos and design/execute test plans/scenarios/cases/scripts.", 1),
("Create automated test scripts using Java, Selenium WebDriver, Appium, TestNG, Page Object Model, Data Driven (from Excel) and Keyword Driven framework to eliminate bottleneck and improve effectiveness throughout Functional, Regression, Integration, Smoke and Cross Browser testing", 1),
("Use build management tool Maven and integrated complete framework with CI tool Jenkins pipeline for automated batch triggering and scheduling of the test cases, resulting in better productivity", 1),
("Run Smoke Regression suite after every sprint build and publish the detailed automated reports (Extent Reports) of the bugs detected to achieve better Test Coverage and Test Accuracy", 1),
("Perform cross browser, cross platform, parallel automation scenarios with Selenium Grid and Sauce Lab Integrations", 1),
("Maintain the automation code and resources in source controls Bitbucket Git, over the time for improvements and new features", 1),
("Review the code prepared by other team mates, analyse the root cause of failed scripts and propose enhancement on current procedures through work-flow analysis", 1),
("Contribute to the identification, management and closure of the system defects in JIRA in order to accept automation criteria", 1),
("Worked on quality assurance and system testing activities (QA/test planning, preparation of test scripts, test execution, defect tracking etc.) using HP ALM/QC", 2),
("Led main functionality sectors of multiple usability improvement projects, involved with functional, user interface, compatible, exploratory, smoke, sanity and regression testing, security testing and user acceptance testing against business requirement document", 2),
("Identified automation scenarios and developed web automation test suites using Selenium WebDriver, Java with Page Object Model (POM) design and Data Driven from database on TestNG Platform", 2),
("Responsible for verifying web service APIs using Rest Assured, Postman under BDD Cucumber framework with POJO class converting JSON request and response body", 2),
("Wrote custom SQL queries extensively using joins, grouping and aggregations to retrieve data for data validation on backend in Oracle database", 2),
("Advised and provided QA expertise and technical innovative insights for product business workflow construction, not only from technical development activities perspective, but also from business enhancement and prototype design perspective", 2),
("Inspired trust by being open, honest and direct in my communication, meeting my commitments and behaving ethically in all my dealings, stay current on established tools, techniques and technologies", 2),
("Strategized and executed quality assurance projects assigned by Project Manager and QA manager, developed and maintained traceability matrix and created test strategies, test plans and test cases for all product offerings to cover functional/non-functional business user stories’ acceptance criteria", 3),
("Provided support in the performance testing using JMeter, including creating Thread Groups and testing Web Application for various loads on key business scenarios and generating necessary graphs/reports", 3),
("Provided support in the performance testing using JMeter, including creating Thread Groups and testing Web Application for various loads on key business scenarios and generating necessary graphs/reports", 3),
("Provided lifecycle management of manual and automated testing projects using QC and Developed QTP/UFT scripts with object model and description programming, and handled dynamic objects using VB-script and regular expression in QTP/UFT",4),
("Used need base SQL queries like select, insert, update, delete and joining multiple tables based on business requirement to conduct backend testing",4),
("Recognized for developing excellent test plans/scripts and rapidly discovering defects, Wrote custom SQL queries extensively using joins, grouping and aggregations to retrieve data for data validation on backend in Oracle database",4),
("Provided L1 and L2 supports of more than 20 banking applications globally and carter to global users, provided supports for middle and back office systems, such as Strategic Middle Office, Netting System, Global Settlement System", 5),
("Involved in test plan development, test cases design, test data preparation, test execution, test case update and issue verification by using the business requirement document to make sure to fit the deadlines", 5),
("Responsible for executing QTP automated test scripts and generating testing reports for each build", 5);

CREATE TABLE IF NOT EXISTS Contacts (
    contact_id int NOT NULL AUTO_INCREMENT,
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255),
    phone_number varchar(255),
    subject varchar(255),
    comments varchar(255),
    person_id int NOT NULL,
    PRIMARY KEY (contact_id),
    FOREIGN KEY (person_id) REFERENCES Profiles (person_id)
);

INSERT INTO Contacts (first_name, last_name, email, phone_number, subject, comments, person_id )
VALUES 
("Serena", "Fan", "sisihang@gmail.com", "6475050768", "this is a subject", "this is a comments", 1);

4. To install the project with "npm install"
5. To start BE with "npm start"